# kubernetes
This is a sample microservice based project 
microservices :
customer-service 
product-service
order-service

Technologies:
spring-boot
kubernetes
helm
jpa
hibernate
istio 


hystrix
feign
ribbon
actuator
